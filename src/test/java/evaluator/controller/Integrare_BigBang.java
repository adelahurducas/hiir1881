package evaluator.controller;

import evaluator.exception.MyException;
import evaluator.main.StartApp;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class Integrare_BigBang {
    private AppController controller3, ctrl;
    private evaluator.model.Test test;
    private evaluator.model.Statistica statistica;


    @Before
    public void setUp() throws Exception{
        String file1=new File("src/main/java/evaluator/test3.txt").getAbsolutePath();
        String file2=new File("src/main/java/evaluator/intrebari1.txt").getAbsolutePath();
        controller3=new AppController(file1);
        ctrl=new AppController(file2);
    }

    @org.junit.Test
    public void test3() throws Exception {
        try {
            test=controller3.createNewTest();
        }catch (MyException e){
            System.out.println(e);
        }
        assertEquals(test.getIntrebari().size(),5);

    }


    //test valid Lab-04 WBT
    @org.junit.Test
    public void test5() throws Exception {
        try {
            statistica=controller3.getStatistica();
        }catch (MyException e){
            System.out.println(e);
        }
        assertEquals(statistica.getIntrebariDomenii().size(),5);

    }

    @Test
    public void TC1_ECP() throws Exception {
        Intrebare intr=new Intrebare("Ce este la pranz?","1)Nimic","2)Putina manacre","3)Ciorba","2","Culinar");
        String filename =new File("src/main/java/evaluator/intrebari3.txt").getAbsolutePath();
        Intrebare actual_result= ctrl.addNewIntrebare(intr, filename);
        Intrebare expexted_result=intr;
        assertEquals(actual_result,expexted_result);

    }

    @Test
    public void integrare()throws Exception{
        //P -> B -> C -> A
        test3();
        test5();
        TC1_ECP();
    }
}
