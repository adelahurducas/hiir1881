package evaluator.controller;

import evaluator.exception.MyException;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class AppControllerTest {
    private AppController controller1,controller2,controller3,controller4;
    private evaluator.model.Test test;
    private evaluator.model.Statistica statistica;

    @Before
    public void setUp() throws Exception{

        String filename1=new File("src/main/java/evaluator/test1.txt").getAbsolutePath();
        String filename2=new File("src/main/java/evaluator/test2.txt").getAbsolutePath();
        String filename3=new File("src/main/java/evaluator/test3.txt").getAbsolutePath();
        String filename4=new File("src/main/java/evaluator/test4.txt").getAbsolutePath();


        controller1=new AppController(filename1);
        controller2=new AppController(filename2);
        controller3=new AppController(filename3);
        controller4=new AppController(filename4);
    }

    //test nonvalid Lab-03 WBT
    @org.junit.Test
    public void test1() throws Exception {

        try {
            test=controller1.createNewTest();
        }catch (MyException e){
            System.out.println(e);
        }

        assertEquals(null,test);

    }

    //test nonvalid Lab-03 WBT
    @org.junit.Test
    public void test2() throws Exception {

        try {
            test=controller2.createNewTest();
        }catch (MyException e){
            System.out.println(e);
        }
        assertEquals(null,test);

    }

    //test valid Lab-03 WBT
    @org.junit.Test
    public void test3() throws Exception {
        try {
            test=controller3.createNewTest();
        }catch (MyException e){
            System.out.println(e);
        }
        assertEquals(test.getIntrebari().size(),5);

    }

    //test nonvalid Lab-04 WBT
    @org.junit.Test
    public void test4() throws Exception {
        try {
            statistica=controller4.getStatistica();
        }catch (MyException e){
            System.out.println(e);
        }
        assertEquals(null,statistica);

    }


    //test valid Lab-04 WBT
    @org.junit.Test
    public void test5() throws Exception {
        try {
            statistica=controller3.getStatistica();
        }catch (MyException e){
            System.out.println(e);
        }
        assertEquals(statistica.getIntrebariDomenii().size(),5);

    }

}