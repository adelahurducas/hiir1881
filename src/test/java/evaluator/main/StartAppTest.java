package evaluator.main;

import evaluator.controller.AppController;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class StartAppTest {

    private AppController ctrl;
    @Before
    public void setUp() throws Exception{
        String file=new File("src/main/java/evaluator/intrebari.txt").getAbsolutePath();
        ctrl=new AppController(file);
//        ctrl=new AppController("C:\\Users\\Adela\\Documents\\VVSS\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebari.txt");
    }

    @Test
    public void TC1_ECP() throws Exception {
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1)Nimic","2)Putina manacre","3)Ciorba","2","Culinar",ctrl);
        int expexted_result=0;
        assertEquals(actual_result,expexted_result);

    }
    @Test
    public void TC2_ECP() throws Exception {
        int actual_result=StartApp.addIntrebare("ce este la pranz?","1)Nimic","2)Putina manacre","3)Ciorba","2","Mancare",ctrl);
        int expexted_result=1;
        assertEquals(actual_result,expexted_result);

    }

    @Test
    public void TC3_ECP() throws Exception {
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","","2)Putina manacre","3)Ciorba","2","Mancare",ctrl);
        int expexted_result=1;
        assertEquals(actual_result,expexted_result);

    }
    @Test
    public void TC4_ECP() throws Exception {
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1)Nimic","2. Putina manacre","3)Ciorba","2","Mancare",ctrl);
        int expexted_result=1;
        assertEquals(actual_result,expexted_result);

    }
    @Test
    public void TC5_ECP() throws Exception {
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1)Nimic","2)Putina manacre","3)Ciorba","5","Mancare",ctrl);
        int expexted_result=1;
        assertEquals(actual_result,expexted_result);

    }
    @Test
    public void TC1_BVA()throws Exception{
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1)Nimic","2)Putina manacre","3)Ciorba","1","Mancare",ctrl);
        int expexted_result=0;
        assertEquals(actual_result,expexted_result);
    }

    @Test
    public void TC2_BVA()throws Exception{
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1)Nimic","2)Putina manacre","3)Ciorba","-1","Mancare",ctrl);
        int expexted_result=1;
        assertEquals(actual_result,expexted_result);

    }
    @Test
    public void TC3_BVA()throws Exception{
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1)Nimic","2)Putina manacre","3)Ciorba","2","Maaaaaaaaaancareeeeeeeeeeeeeee",ctrl);
        int expexted_result=0;
        assertEquals(actual_result,expexted_result);

    }
    @Test
    public void TC4_BVA()throws Exception{
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1","2)Putina manacre","3)Ciorba","2","Mancare",ctrl);
        int expexted_result=1;
        assertEquals(actual_result,expexted_result);

    }
    @Test
    public void TC5_BVA()throws Exception{
        int actual_result=StartApp.addIntrebare("Ce este la pranz?","1)Nimic","2) Putina manacre","3)Ciorba","2","Maaaaaaaaaancareeeeeeeeeeeeeeee",ctrl);
        int expexted_result=1;
        assertEquals(actual_result,expexted_result);

    }

}