package evaluator.exception;

public class MyException extends Exception {

	private static final long serialVersionUID = 1L;
	String message;
	public MyException(String message) {
		this.message=message;
	}
	public String getMessage(){
		return this.message;
	}

}
