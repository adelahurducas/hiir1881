package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import evaluator.exception.MyException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

    private static final String file = "C:\\Users\\Adela\\Documents\\VVSS\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\evaluator\\intrebari.txt";

    public static int addIntrebare(String enunt,String varianta1,String varianta2,String varianta3,String variantaCorecta,String domeniu,AppController appController){

        Intrebare intrebare;
        try {
            intrebare = new Intrebare(enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu);
        } catch (MyException e) {
            intrebare = null;
            System.out.println(e.getMessage());
            return 1;
        }
        if (intrebare != null) {
            try {
                appController.addNewIntrebare(intrebare,file);
            } catch (MyException e) {
                System.out.println(e.getMessage());

            }
        }
        System.out.println("Intrebare adugata");
        return 0;
    }

    public static void main(String[] args) throws IOException {

        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));

        AppController appController = new AppController(file);

        boolean activ = true;
        String optiune = null;

        while (activ) {

            System.out.println("");
            System.out.println("1.Adauga intrebare");
            System.out.println("2.Creeaza test");
            System.out.println("3.Statistica");
            System.out.println("4.Exit");
            System.out.println("");

            optiune = console.readLine();

            switch (optiune) {
                case "1":
                    String enunt, varianta1, varianta2, varianta3, variantaCorecta, domeniu;
                    Scanner scanner = new Scanner(System.in);
                    System.out.println("Introduceti enuntul: ");
                    enunt = scanner.nextLine();
                    System.out.println("Introduceti varianta1: ");
                    varianta1 = scanner.nextLine();
                    System.out.println("Introduceti varianta2: ");
                    varianta2 = scanner.nextLine();
                    System.out.println("Introduceti varianta3: ");
                    varianta3 = scanner.nextLine();
                    System.out.println("Introduceti varianta corecta:");
                    variantaCorecta = scanner.nextLine();
                    System.out.println("Introduceti domeniul:");
                    domeniu = scanner.nextLine();
                    if(addIntrebare(enunt,varianta1,varianta2,varianta3,variantaCorecta,domeniu,appController)==0){
                        System.out.println("Intrebare adugata");
                    }
                    break;

                case "2":
                    try {
                        appController.createNewTest();
                    } catch (MyException e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case "3":
                    appController.loadIntrebariFromFile(file);
                    Statistica statistica;
                    try {
                        statistica = appController.getStatistica();
                        System.out.println(statistica);
                    } catch (MyException e) {
                        System.out.println(e.getMessage());
                    }
                    break;

                case "4":
                    activ = false;
                    break;
                default:
                    break;
            }
        }

    }

}
