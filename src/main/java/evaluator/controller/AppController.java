package evaluator.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import evaluator.exception.MyException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.model.Test;
import evaluator.repository.IntrebariRepository;

public class AppController {
	
	private IntrebariRepository intrebariRepository;
	private String filename;
	public AppController(String f) {
		this.filename=f;
		intrebariRepository = new IntrebariRepository(filename);
	}
	
	public Intrebare addNewIntrebare(Intrebare intrebare, String filename) throws MyException{
		
		intrebariRepository.addIntrebare(intrebare,filename);
		
		return intrebare;
	}
	
	public boolean exists(Intrebare intrebare){
		return intrebariRepository.exists(intrebare);
	}
	
	public Test createNewTest() throws MyException{
		
		if(intrebariRepository.getIntrebari().size() < 5)
			throw new MyException("Nu exista suficiente intrebari pentru crearea unui test!(5)");
		
		if(intrebariRepository.getNumberOfDistinctDomains() < 5)
			throw new MyException("Nu exista suficiente domenii pentru crearea unui test!(5)");
		
		List<Intrebare> testIntrebari = new LinkedList<Intrebare>();
		List<String> domenii = new LinkedList<String>();
		Intrebare intrebare;
		Test test = new Test();
		
		while(testIntrebari.size() != 5){
			intrebare = intrebariRepository.pickRandomIntrebare();
			
			if(!testIntrebari.contains(intrebare) && !domenii.contains(intrebare.getDomeniu())){
				testIntrebari.add(intrebare);
				domenii.add(intrebare.getDomeniu());
			}
			
		}
		
		test.setIntrebari(testIntrebari);
		return test;

	}
	
	public void loadIntrebariFromFile(String f) throws IOException
	{
		if (intrebariRepository.loadIntrebariFromFile(f).isEmpty()){
			System.out.println("Nu exista intrebari in sistem!");
		}else {
			intrebariRepository.setIntrebari(intrebariRepository.loadIntrebariFromFile(f));
		}
	}
	
	public Statistica getStatistica() throws MyException{
		
		if(intrebariRepository.getIntrebari().isEmpty())
			throw new MyException("Repository-ul nu contine nicio intrebare!");
		
		Statistica statistica = new Statistica();
		for(String domeniu : intrebariRepository.getDistinctDomains()){
			statistica.add(domeniu, intrebariRepository.getNumberOfIntrebariByDomain(domeniu));
		}
		
		return statistica;
	}

}
