package evaluator.repository;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;


import evaluator.exception.MyException;
import evaluator.model.Intrebare;

public class IntrebariRepository {
	
	private List<Intrebare> intrebari;
	private String filename;
	public IntrebariRepository(String filename) {
		this.filename=filename;
		setIntrebari(new LinkedList<Intrebare>());
		intrebari=loadIntrebariFromFile(filename);

	}
	
	public void addIntrebare(Intrebare i, String filename) throws MyException {
		loadIntrebariFromFile(filename);
		if (exists(i))
			throw new MyException("Intrebarea deja exista!");
		intrebari.add(i);
		writeToFile(filename);
	}
	
	public boolean exists(Intrebare i){
		for(Intrebare intrebare : intrebari)
			if(intrebare.equals(i))
				return true;
		return false;
	}
	
	public Intrebare pickRandomIntrebare(){
		Random random = new Random();
		return intrebari.get(random.nextInt(intrebari.size()));
	}
	
	public int getNumberOfDistinctDomains(){
		return getDistinctDomains().size();
		
	}
	
	public Set<String> getDistinctDomains(){
		Set<String> domains = new TreeSet<String>();
		for(Intrebare intrebre : intrebari)
			domains.add(intrebre.getDomeniu());
		return domains;
	}
	
	public List<Intrebare> getIntrebariByDomain(String domain){
		List<Intrebare> intrebariByDomain = new LinkedList<Intrebare>();
		for(Intrebare intrebare : intrebari){
			if(intrebare.getDomeniu().equals(domain)){
				intrebariByDomain.add(intrebare);
			}
		}
		
		return intrebariByDomain;
	}
	
	public int getNumberOfIntrebariByDomain(String domain){
		return getIntrebariByDomain(domain).size();
	}
	
	public List<Intrebare> loadIntrebariFromFile(String f){
		
		List<Intrebare> intrebari = new LinkedList<Intrebare>();
		BufferedReader br = null; 
		String line = null;
		List<String> intrebareAux;
		Intrebare intrebare;
		
		try{
			br = new BufferedReader(new FileReader(f));
			line = br.readLine();
			while(line != null){
				intrebareAux = new LinkedList<String>();
				while(line != null && !line.equals("##")){
					intrebareAux.add(line);
					line = br.readLine();
				}
				intrebare = new Intrebare();
				intrebare.setEnunt(intrebareAux.get(0));
				intrebare.setVarianta1(intrebareAux.get(1));
				intrebare.setVarianta2(intrebareAux.get(2));
				intrebare.setVarianta3(intrebareAux.get(3));
				intrebare.setVariantaCorecta(intrebareAux.get(4));
				intrebare.setDomeniu(intrebareAux.get(5));
				intrebari.add(intrebare);
				line = br.readLine();
			}
		
		}
		catch (IOException e) {
			System.out.println(e);
		}
		finally{
			try {
				if (br !=null){
					br.close();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
		}
		
		return intrebari;
	}

	public void writeToFile(String filename){
		BufferedWriter bw = null;
		List<Intrebare> intrebari= getIntrebari();
		try{
			bw = new BufferedWriter(new FileWriter(filename));
			for (Intrebare i : intrebari){
				bw.write(i.getEnunt()+"\n");
				bw.write(i.getVarianta1()+"\n");
				bw.write(i.getVarianta2()+"\n");
				bw.write(i.getVarianta3()+"\n");
				bw.write(i.getVariantaCorecta()+"\n");
				bw.write(i.getDomeniu()+"\n");
				bw.write("##\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if(bw!=null){
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<Intrebare> getIntrebari() {
		return intrebari;
	}

	public void setIntrebari(List<Intrebare> intrebari) {
		this.intrebari = intrebari;
	}
	
}
